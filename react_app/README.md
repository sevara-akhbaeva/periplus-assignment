### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br>


### `npm run storybook`

Storybook should start on [http://localhost:9009] in the browser.

### In this directory there is a documentation 'Assignment.docx' explaining my approach to do this assignment.