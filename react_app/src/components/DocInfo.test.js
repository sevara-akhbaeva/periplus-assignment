import React from 'react'
import {shallow} from 'enzyme'
import Typography from '@material-ui/core/Typography'
import DocInfo from './DocInfo';
import data from '../data.json'

const docInfo = data.data[0]; 

// check whether 5 <Typography /> components were rendered
describe('<DocInfo />', () => {
    it('renders 5 <Typography /> components', () => {
        const wrapper = shallow(<DocInfo fileInfo={docInfo}><div/></DocInfo>);
        expect(wrapper.find(Typography)).toHaveLength(5);
    });
    
})