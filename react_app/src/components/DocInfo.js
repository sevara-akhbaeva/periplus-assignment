import React from 'react';
import Typography from '@material-ui/core/Typography';

function DocInfo(props){
    // read info from properties
    const docName = props.fileInfo.docName;
    const fileInfo = props.fileInfo;
    
    return (
        <div>
            {/* display file information */}
            <Typography variant='subtitle2' style={{fontWeight:'bolder'}} color='primary' gutterBottom>
                {docName}
            </Typography>
            
            <Typography style={{color:'grey'}} variant='caption' >
                Date : {fileInfo.date}
            </Typography>
            <br/>
            <Typography style={{color:'grey'}} variant='caption' >
                Org. ID : {fileInfo.orgId}
            </Typography>
            <br/>
            <Typography style={{color:'grey'}} variant='caption' >
                Pages : {fileInfo.pages}
            </Typography>
            <br/>
            <Typography style={{color:'grey'}} variant='caption' >
                Type : {fileInfo.type}
            </Typography>
        </div>
    )
}

export default DocInfo;