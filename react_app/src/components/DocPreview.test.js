import React from 'react'
import {shallow} from 'enzyme'
import DocPreview from './DocPreview'
import Paper from '@material-ui/core/Paper'
import data from '../data.json'

const fileInfo = data.data[0]; 

// check whether one <Paper/> component was rendered
describe('<DocPreview />', () => {
    const wrapper = shallow(<DocPreview fileInfo={fileInfo} withToggleButton={true}/>);

    it('renders one <Paper /> component', () => {
        // const wrapper = shallow(<DocPreview />);
        expect(wrapper.find(Paper)).toHaveLength(1);
    });
})

