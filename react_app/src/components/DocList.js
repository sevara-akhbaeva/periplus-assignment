import React from 'react'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import IconButton from '@material-ui/core/IconButton'
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft'
import ArrowRightIcon from '@material-ui/icons/ArrowRight'
import Paper from '@material-ui/core/Paper'
import {Document, Page} from 'react-pdf'
import '../style.css'
import GridListTileBar from '@material-ui/core/GridListTileBar'

// import Doc from './Doc'
import {useState} from 'react'
import { Typography } from '@material-ui/core'

// returns array with 'gridNum' number of elements
// starting with 'firstPage' value, 
// but maximum value for last element = numPages
// required to generate index of loaded pages
function generateArray(firstPage, numPages, gridNum){
    let lastPage = (firstPage + gridNum)
    let arr = []
    for(let i = firstPage, idx = 0; i < lastPage && i <= numPages; i++, idx++){
        arr[idx] = i
    }
    return arr
}

function DocList(props){
    const filename = props.filename
    const gridNum = props.gridNum
    const [numPages, setNumPages] = useState(0)
    const [firstPage, changeFirstPage] = useState(1)

    // generate indices of pages
    let pages  = generateArray(firstPage, numPages, gridNum)

    // generate list of pages
    let list = pages.map((page) => {
        return (
        <Paper key={page} style={{padding : 0, marginRight: 5}}>
            <GridListTile>
                    <Page pageNumber={page} />
                    <GridListTileBar
                    title={page}/>
            </GridListTile>
        </Paper>
        )
    })

    return(
        <div>
            <Typography color='primary' align='center' variant='h5' gutterBottom>{filename}</Typography>
            
            {/* NOT display left button if there is NO more pages to the left*/}
            {(firstPage !== 1) ? 
            <IconButton color='primary' style={{float:'left'}} 
            onClick={() => changeFirstPage(firstPage - gridNum)}>
                <ArrowLeftIcon fontSize="large" />
            </IconButton> : null}
            {/* NOT display right button if there is NO more pages to the right*/}
            {(pages[pages.length - 1] !== numPages) ?
            <IconButton color='primary' style={{float:'right'}}
            onClick={() => changeFirstPage(firstPage + gridNum)}>
                <ArrowRightIcon fontSize="large" />
            </IconButton> : null}

            {/* display the list of pages */}
            <Document file={filename} onLoadSuccess={({numPages}) => setNumPages(numPages)} noData={<h4>Error in loading PDF file...</h4>}>
                <GridList style={{flexWrap: 'nowrap'}} cols={gridNum}>
                    {list}
                </GridList>  
            </Document>
        </div>
    )
}

export default DocList