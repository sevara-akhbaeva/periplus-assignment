import React from 'react';
import DocInfo from './DocInfo.js';
import DocList from './DocList.js';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper'
import {useState} from 'react'
import { Button } from '@material-ui/core';

function DocPreview(props){
    const filenames = ['./example.pdf', './example_large.pdf']
    const [fileIdx, toggleFilename] = useState(0) // hooks for toggle files
    
    // when button is clicked to change the file
    function clickHandle(){
        let newIdx = (fileIdx === 0) ? 1 : 0 // toggle file index
        toggleFilename(newIdx) // set new value to state
    }

    return (
    <Paper style={{margin:5}}>
        <Grid container  spacing={0}>
                <Grid item style={{background: '#E6E6FA', padding: 24}} xs={12} sm={3} md={2}>
                    {/* left side with file info, pass data read from JSON file*/}
                    <DocInfo fileInfo={props.fileInfo} /> 

                    {/* check whether toggle button is required */}
                    {props.withToggleButton ? 
                    <Button style={{position:'static', marginTop:10, top:0}} variant="outlined" 
                    size="small" color="primary"
                    onClick={clickHandle}>
                        Open {fileIdx === 0 ? filenames[1] : filenames[0]}
                    </Button> : null}

                </Grid>

                {/*right side with list of file pages*/}
                <Grid item style={{padding: 24}} xs={12} sm={9} md={10}>
                    {/*pass to component filename and number of pages displayed on the screen*/}
                    <DocList filename={filenames[fileIdx]} gridNum={7}/>
                </Grid>
        </Grid>
    </Paper>
    )
    
}

export default DocPreview;