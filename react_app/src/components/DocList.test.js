import React from 'react'
import {shallow} from 'enzyme'
import IconButton from '@material-ui/core/IconButton'
import DocList from './DocList';

// check whether on the start of the page there is one <Button/> (right one)
describe('<DocList />', () => {
    it('check whether there is only one button', () => {
        const wrapper = shallow(<DocList filename={'../example.pdf'} gridNum={7}></DocList>);
        expect(wrapper.find(IconButton)).toHaveLength(1);
    });
    
})