import React from 'react';
import {storiesOf} from '@storybook/react'
import DocPreview from '../components/DocPreview';
import data from '../data.json';

const fileInfo = data.data[0];
storiesOf('DocPreview', module)
.add('with toggle button', () => <DocPreview fileInfo={fileInfo} withToggleButton={true} />)
.add('without toggle button', () => <DocPreview fileInfo={fileInfo} withToggleButton={false} />)
