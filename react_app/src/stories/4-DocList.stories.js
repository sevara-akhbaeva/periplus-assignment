import React from 'react';
import {storiesOf} from '@storybook/react'
import DocList from '../components/DocList'

storiesOf('DocList', module)
.add('Number of Grid tiles=7 (default)', () => <DocList filename={'./example.pdf'} gridNum={7}/>)
.add('Number of Grid tiles=6', () => <DocList filename={'./example.pdf'} gridNum={6} />)
.add('Number of Grid tiles=8', () => <DocList filename={'./example.pdf'} gridNum={8} />)
