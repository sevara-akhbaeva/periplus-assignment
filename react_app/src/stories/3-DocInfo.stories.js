import React from 'react';
import {storiesOf} from '@storybook/react'
import DocInfo from '../components/DocInfo';
import data from '../data.json';

const fileInfo = data.data[0];
const customData = {
    "docName": "Example DATA",
    "date": "12/10/2019",
    "orgId": "1000",
    "pages": 10,
    "type": "Book",
    "path": "./example.pdf"
}
storiesOf('DocInfo')
.add('with data read from JSON file', () => <DocInfo fileInfo={fileInfo} />)
.add('with data typed by me ', () => <DocInfo fileInfo={customData} />)