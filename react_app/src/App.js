import React from 'react';
import DocPreview from './components/DocPreview.js';
import data from './data.json'; // load JSON file to variable data

function App() {
  const fileInfo = data.data[0]; // extract object with necessary information
  return (
    <div>
      <DocPreview fileInfo={fileInfo} withToggleButton={true} />
    </div>
  );
}

export default App;
